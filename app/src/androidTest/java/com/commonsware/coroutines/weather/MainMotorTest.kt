/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Kotlin Coroutines_

  https://commonsware.com/Coroutines
*/

package com.commonsware.coroutines.weather

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.schedulers.TestScheduler
import io.reactivex.subjects.PublishSubject
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.threeten.bp.format.DateTimeFormatter


private val TEST_MODEL = ObservationModel(
  id = "https://api.weather.gov/stations/KDCA/observations/2019-07-29T16:52:00+00:00",
  timestamp = "2019-07-29T16:52:00+00:00",
  icon = "https://api.weather.gov/icons/land/day/sct?size=medium",
  temperatureCelsius = 33.30000000000001,
  windDirectionDegrees = 40.0,
  windSpeedMetersSecond = 2.6,
  barometricPressurePascals = 101690.0
)
private val TEST_ROW_STATE = RowState(
  icon = TEST_MODEL.icon,
  timestamp = "2019-07-29T12:52:00-04:00",
  temp = "33.3 ℃",
  wind = "2.6 m/s @ 40°",
  pressure = "101690 Pa"
)
private val TEST_MODEL_2 = ObservationModel(
  id = "https://api.weather.gov/stations/KDCA/observations/2019-07-29T17:52:00+00:00",
  timestamp = "2019-07-29T17:52:00+00:00",
  icon = "https://api.weather.gov/icons/land/day/sct?size=medium",
  temperatureCelsius = 33.40000000000001,
  windDirectionDegrees = 40.1,
  windSpeedMetersSecond = 2.7,
  barometricPressurePascals = 101691.0
)
private val TEST_ROW_STATE_2 = RowState(
  icon = TEST_MODEL.icon,
  timestamp = "2019-07-29T13:52:00-04:00",
  temp = "33.4 ℃",
  wind = "2.7 m/s @ 40°",
  pressure = "101691 Pa"
)
private val TEST_ERROR = IllegalStateException("ick!")

@RunWith(AndroidJUnit4::class)
class MainMotorTest {
  @get:Rule
  val instantTaskExecutorRule = InstantTaskExecutorRule()
  @get:Rule
  val androidSchedulerRule = AndroidSchedulerRule(TestScheduler())

  private val repo: IObservationRepository = mock()

  @Test
  fun initialLoad() {
    whenever(repo.load()).thenReturn(Observable.just(listOf(TEST_MODEL)))

    val underTest = makeTestMotor()
    val initialState = underTest.states.value

    assertThat(initialState is MainViewState.Loading, equalTo(true))
    androidSchedulerRule.scheduler.triggerActions()

    val state = underTest.states.value as MainViewState.Content

    assertThat(state.observations.size, equalTo(1))
    assertThat(state.observations[0], equalTo(TEST_ROW_STATE))
  }

  @Test
  fun initialLoadError() {
    whenever(repo.load()).thenReturn(Observable.error(TEST_ERROR))

    val underTest = makeTestMotor()
    val initialState = underTest.states.value

    assertThat(initialState is MainViewState.Loading, equalTo(true))
    androidSchedulerRule.scheduler.triggerActions()

    val state = underTest.states.value as MainViewState.Error

    assertThat(TEST_ERROR, equalTo(state.throwable))
  }

  @Test
  fun refresh() {
    val testSubject: PublishSubject<List<ObservationModel>> =
      PublishSubject.create()

    whenever(repo.load()).thenReturn(testSubject)
    whenever(repo.refresh()).thenReturn(Completable.complete())

    val underTest = makeTestMotor()
    val initialState = underTest.states.value

    assertThat(initialState is MainViewState.Loading, equalTo(true))

    testSubject.onNext(listOf(TEST_MODEL))
    androidSchedulerRule.scheduler.triggerActions()

    underTest.refresh()

    testSubject.onNext(listOf(TEST_MODEL, TEST_MODEL_2))
    androidSchedulerRule.scheduler.triggerActions()

    val state = underTest.states.value as MainViewState.Content

    assertThat(state.observations.size, equalTo(2))
    assertThat(state.observations[0], equalTo(TEST_ROW_STATE))
    assertThat(state.observations[1], equalTo(TEST_ROW_STATE_2))
  }

  @Test
  fun clear() {
    val testSubject: PublishSubject<List<ObservationModel>> =
      PublishSubject.create()

    whenever(repo.load()).thenReturn(testSubject)
    whenever(repo.clear()).thenReturn(Completable.complete())

    val underTest = makeTestMotor()
    val initialState = underTest.states.value

    assertThat(initialState is MainViewState.Loading, equalTo(true))

    testSubject.onNext(listOf(TEST_MODEL))
    androidSchedulerRule.scheduler.triggerActions()

    underTest.clear()

    testSubject.onNext(listOf())
    androidSchedulerRule.scheduler.triggerActions()

    val state = underTest.states.value as MainViewState.Content

    assertThat(state.observations.size, equalTo(0))
  }

  private fun makeTestMotor() = MainMotor(
    repo,
    DateTimeFormatter.ISO_OFFSET_DATE_TIME,
    InstrumentationRegistry.getInstrumentation().targetContext
  )
}
